#
# Cookbook Name:: haproxy
# Recipe:: default
#
# Copyright 2009, Opscode, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

if platform?("ubuntu")
  #commented_line = /^#\s+(session\s+\w+\s+pam_limits\.so)\b/m
  
   commented_line_1 =/^#(deb http:\/\/.*trusty-backports.*)/m
   commented_line_2 =/^#(deb-src http:\/\/.*trusty-backports.*)/m

  
  ruby_block "uncomment source in apt source list" do
  block do
    rc = Chef::Util::FileEdit.new("/etc/apt/sources.list")	
     rc.search_file_replace(commented_line_1, '\1')
	 rc.search_file_replace(commented_line_2, '\1')
    rc.write_file
  end
  only_if do
    ::File.exists?("/etc/apt/sources.list")
  end
  end

  execute "apt-get-update" do
	command "sudo apt-get update"
	ignore_failure true
	action :nothing
  end

end

package 'haproxy' do
  version node[:haproxy][:version]
  action :install
end

if platform?('debian','ubuntu')
  template '/etc/default/haproxy' do
    source 'haproxy-default.erb'
    owner 'root'
    group 'root'
    mode 0644
  end
end

directory "/etc/ssl/private" do
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end

cookbook_file "service-flow.com.pem" do
  path "/etc/ssl/private/service-flow.com.pem"
  action :create_if_missing
end

include_recipe 'haproxy::service'

template '/etc/haproxy/haproxy.cfg' do
  source 'haproxy.cfg.erb'
  owner 'root'
  group 'root'
  mode 0644
  notifies :restart, "service[haproxy]"
end

service 'haproxy' do
  action [:enable, :start]
end
